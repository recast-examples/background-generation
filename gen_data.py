import ROOT

def main():
  
    r = ROOT.TRandom()

    h_bkg = ROOT.TH1F("h_bkg","h_bkg",20,0,500)
    h_bkg.Sumw2()
    
    h_dat = ROOT.TH1F("h_dat","h_dat",20,0,500)
    h_dat.Sumw2()

    for i in range(40000):
      val = r.Exp(150)
      h_bkg.Fill(val)
      

    for i in range(40000):
      val = r.Exp(150)
      h_dat.Fill(val)
      

    fout = ROOT.TFile("external_data.root","RECREATE")
    h_bkg.Write("background")
    h_dat.Write("data")
    fout.Close()
    
if __name__ == '__main__':
    main()
